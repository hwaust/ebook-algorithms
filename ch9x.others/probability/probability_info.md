# 概率学简介







## 百分位数 (percentile)



Q: 编写一个Python函数，get_percentile(datalist, p)，能够求一组数据datalist中，最大的前百分之p。

A: 以下是一个Python函数的示例代码，可以获取一组数据中最大的前百分之p：

```
python复制代码def get_percentile(datalist, p):
    sorted_data = sorted(datalist, reverse=True) # 将数据从大到小排序
    n = len(sorted_data)
    k = int(n * p / 100) # 计算需要选取的个数
    return sorted_data[:k] # 返回前k个元素
```

该函数首先对数据列表进行降序排序（即从大到小），然后计算出需要选取的前百分之p的元素数量k。最后，它返回列表的前k个元素，这些元素就是最大的前百分之p。注：如果需要获取最大的前k个元素，只需将公式中的“int(n * p / 100)”改为“k”。

以下是一个使用示例：

```
python复制代码data = [3, 7, 1, 9, 4, 6, 8, 2, 5]
percentile = 40

result = get_percentile(data, percentile)
print(f"最大的前{percentile}%: {result}")
```

该示例计算了数据列表 `[3, 7, 1, 9, 4, 6, 8, 2, 5]` 中最大的前百分之40的元素，并输出结果 `[9, 8, 7]`。



扩展概念 ：四分位数 The first quartile,  second quartile, third quartile and forth quartile.

