# 过拟合与欠拟合



$$
\mathrm{Loss}^{(train)} = \frac{1}{m^{(train)}} || \boldsymbol{X}^{(train)}\boldsymbol{w} - \boldsymbol{y}^{(train)} ||_2^2 \tag{Eq.1}
$$



$$
\mathrm{Loss}^{(test)} = \frac{1}{m^{(test)}} || \boldsymbol{X}^{(test)}\boldsymbol{w} - \boldsymbol{y}^{(test)} ||_2^2 \tag{Eq.2}
$$


## 欠拟合 (Underfitting)
表示无法根据已有样本得到较好的拟合效果，$\hat{f}$ 与 $f$ 的值相差较大， 即 $$\mathrm{Loss}^{(train)}$$在训练过程中，始终无法得到到足够低的误差。造成的原因通常是模型无法对样本数据进行很好地拟合。

## 过拟合 (Overfitting)
过拟合则表示能够在，$\hat{f}^{(train)}$ 与 $f$ 的值能够得到较好的预期， 即 $$\mathrm{Loss}^{(test)}$$在训练过程中，能够得到到足够低的误差，但是使用测试集进行测试时，性能指标不好，准确性相对训练结果降低过多。

## 未知数据测试 (Unseen Sample Test)
如果即没有发生过拟合，也没有发生欠拟合，但是在使用新的数据训练时，正确率差的比较大，则认为模型的泛化(generalization)能力较弱。