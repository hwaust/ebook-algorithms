Part I: 本章简介机器学习的基础知识。



# 推荐阅读

* [Mathematics for Machine Learning](https://mml-book.github.io/) 一本不错书，提供PDF下载。
* [Machine Learning Mathematics](https://www.geeksforgeeks.org/machine-learning-mathematics/) GeeksForGeeks 网站简介
* [Mathematics of Machine Learning: An introduction.pdf](https://oar.princeton.edu/bitstream/88435/pr1cg32/1/MachineLearningMathematics.pdf) 一份不错的PDF版的数据符号简介。

