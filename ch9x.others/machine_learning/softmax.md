---
<font face="微软雅黑" size="8">SoftMax函数</font>
郝伟 2021/04/19

[TOC]

# 1 前言
SoftMax函数是常用的一个机器学习中的函数，网上的解释很多，但是大都是从论文中复制翻译过来，不容易理解。所以本文以示例和代码进行解释，非常简单直白式，让读者能够快速理解。

# 2 1分钟理解SoftMax

**请从头到尾认真阅读本节，只要你认真看完成了，1分钟即可理解SoftMax函数。**

正文开始：给定三个变量 $x_1=5, x_2=-3, x_3=0.8$，SoftMax计算过程只有两步：

**第1步 对求输入求对e的指数 (结果保留4位)**
$$v_1=e^{x_1}=e^{5}=148.4132$$
$$v_2=e^{x_2}=e^{-3}=0.0498$$
$$v_3=e^{x_3}=e^{0.8}=2.2255$$

**第2步 进行归一化处理**
$$y_1=\textrm{softmax}(x_1)=\frac{v_1}{v_1+v_2+v_3}=\frac{148.4132}{148.4132+0.0498+2.2255}=0.9849$$
$$y_2=\textrm{softmax}(x_2)=\frac{v_2}{v_1+v_2+v_3}=\frac{0.0498}{148.4132+0.0498+2.2255}=0.0003$$
$$y_3=\textrm{softmax}(x_3)=\frac{v_3}{v_1+v_2+v_3}=\frac{2.2255}{148.4132+0.0498+2.2255}=0.0148$$

完成。


# 3 代码示例
以下是使用代码实现的SoftMax函数。
```python
import math

def softmax(xs):
  '''
  函数：softmax 函数，用于数组归一化
  输入：x的序列
  输出：经过计算的y序列
  '''
  ys = []
  sm = sum([math.exp(x) for x in xs])
  for x in xs:
    ys.append(math.exp(x) / sm)
  return ys

# 输出值
xs=[5, -3, 0.8]

# 经过softmax函数计算的输出值
ys = softmax(xs)

# 打印出 xs, ys 和 sum(ys)
print('方法1'.center(60, '*'))
print('xs:', xs)
print('ys:', ys)
print('sum(ys):', sum(ys)) # 和必然为1

print('方法2'.center(60, '*'))
import numpy as np
scores = np.array(xs)
results = np.exp(scores) / np.sum(np.exp(scores))
print('scores:', scores)
print('results:', results)
```

输出结果为
```
****************************方法1*****************************
xs: [5, -3, 0.8]
ys: [0.9849004523128639, 0.0003303972939552459, 0.014769150393180792]
sum(ys): 1.0
****************************方法2*****************************
scores: [ 5.  -3.   0.8]
results: [9.84900452e-01 3.30397294e-04 1.47691504e-02]
```

# 4 小结
SoftMax函数通过引入e的指数的方式，能够将计算的输出值拉开明显的距离，从而选择显著的结果。不过，其计算结果由于使用了指数，所以值可能非常大，有可能会造成数值溢出。


# 附：复杂的学术图片
网上经常看到下面的图片，都是从论文中截图过来的，对初学者非常不友好。在有了以上的理解后，再看会感觉好些。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210419140733865.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzE0NTM2MQ==,size_16,color_FFFFFF,t_70)
或者是这样的：
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021041914074433.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzE0NTM2MQ==,size_16,color_FFFFFF,t_70)