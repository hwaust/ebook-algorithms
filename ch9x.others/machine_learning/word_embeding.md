# 文本表示方法

# 简介

Word embedding 是自然语言处理中的重要环节，它是一些语言处理模型的统称，并不具体指某种算法或模型。Word embedding 的任务是把词转换成可以计算的向量 。从概念上讲，它涉及从每个单词一维的空间到具有更低维度的连续向量空间的数学嵌入。生成这种映射的方法包括神经网络，单词共生矩阵的降维，概率模型，可解释的知识库方法，和术语的显式表示单词出现的上下文。当用作底层输入表示时，单词和短语嵌入已经被证明可以提高NLP任务的性能，例如句法分析和情感分析。


## 	词嵌入（Word Embedding）

### 主流的word embedding算法

* Word2vec
  这是一种基于统计方法来获得词向量的方法，他是 2013 年由谷歌的 Mikolov 提出了一套新的词嵌入方法。
  两种训练模式：

  * 1、通过上下文来预测当前词（CBOW）我的笔记：把词语向量化
  * 2、通过当前词来预测上下文 （Skip-gram）

  想要详细了解 Word2vec，可以看看这篇文章：《一文看懂 Word2vec（基本概念+2种训练模型+5个优缺点）》

* GloVe
  GloVe 是对 Word2vec 方法的扩展，它将全局统计和 Word2vec 的基于上下文的学习结合了起来。想要了解 GloVe 的 三步实现方式、训练方法、和 w2c 的比较。可以看看这篇文章：《GloVe详解》



## One-Hot编码

## 整数编码

## 转词向量的方法（Continuous Bag-of-Word）

### CBOW

### Skip-Gram



根据[4], The pink horse is eating. → [1 0 0 0 0] [0 1 0 0 0] [0 0 1 0 0] [0 0 0 1 0] [0 0 0 0 1]

[Word2Vec](https://www.baeldung.com/cs/ml-word2vec-topic-modeling) is a common technique used in Natural Language Processing. In it, similar words have similar word embeddings; this means that they are close to each other in terms of cosine distance. **There are two main algorithms to obtain a Word2Vec implementation: Continous Bag of Words and Skip-Gram**. These algorithms use neural network models in order to obtain the word vectors.



# 参考资料

[1] 什么是词嵌入| Word Embedding, https://blog.csdn.net/Mr_Pend/article/details/118414836

[2] 莫烦NLP学习系列：把词语向量化, https://blog.csdn.net/Mr_Pend/article/details/118405199

[3] 莫烦NLP学习系列 自然语言迅理教程 https://mofanpy.com/tutorials/machine-learning/nlp/

[4] Word Embeddings: CBOW vs Skip-Gram https://www.baeldung.com/cs/word-embeddings-cbow-vs-skip-gram

[5] Topic Modeling with Word2Vec, https://www.baeldung.com/cs/ml-word2vec-topic-modeling



