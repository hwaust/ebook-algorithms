---
title: "Recursion"
numbersections: true
author: Dr.Hao
pandoc_args: ["--toc", "--toc-depth=4"]
toc:
  depth_from: 1
  depth_to: 4
  ordered: false  
html:
  embed_local_images: true
  embed_svg: true
  offline: false
export_on_save:
  pandoc: true
  html: true
# TM_SELECTED_TEXT: 
# TM_CURRENT_LINE: 
# TM_CURRENT_WORD: 
# TM_LINE_INDEX: 0
# TM_LINE_NUMBER: 1
# TM_FILENAME: recursion.md
# TM_FILENAME_BASE: recursion
# TM_DIRECTORY: /opt/apache-tomcat-8.5.57/webapps/ROOT/ebooks/algorithms
# TM_FILEPATH: /opt/apache-tomcat-8.5.57/webapps/ROOT/ebooks/algorithms/recursion.md
# RELATIVE_FILEPATH: http://121.199.10.158:8107/ebooks/algorithms/recursion.md
# CLIPBOARD: CLIPBOARD
# WORKSPACE_NAME: ROOT
# WORKSPACE_FOLDER: \opt\apache-tomcat-8.5.57\webapps\ROOT
# CURSOR_INDEX: 0
# CURSOR_NUMBER: 1
# REF: https://code.visualstudio.com/docs/editor/userdefinedsnippets
---
<font face="微软雅黑" size="8"> Recursion </font>

[TOC]

# 1 Introduction


## 1.1 What is Recursion? 
The process in which a function calls itself directly or indirectly is called recursion and the corresponding function is called a recursive function. Using a recursive algorithm, certain problems can be solved quite easily. Examples of such problems are Towers of Hanoi (TOH), Inorder/Preorder/Postorder Tree Traversals, DFS of Graph, etc. A recursive function solves a particular problem by calling a copy of itself and solving smaller subproblems of the original problems. Many more recursive calls can be generated as and when required. It is essential to know that we should provide a certain case in order to terminate this recursion process. So we can say that every time the function calls itself with a simpler version of the original problem.

## 1.2 Need of Recursion

Recursion is an amazing technique with the help of which we can reduce the length of our code and make it easier to read and write. It has certain advantages over the iteration technique which will be discussed later. A task that can be defined with its similar subtask, recursion is one of the best solutions for it. For example; The Factorial of a number.


## 1.3 Properties of Recursion
* Performing the same operations multiple times with different inputs.
* In every step, we try smaller inputs to make the problem smaller.
* Base condition is needed to stop the recursion otherwise infinite loop will occur.

# 2 Algorithm: Steps
The algorithmic steps for implementing recursion in a function are as follows:

* Step1 - Define a base case: Identify the simplest case for which the solution is known or trivial. This is the stopping condition for the recursion, as it prevents the function from infinitely calling itself.

* Step2 - Define a recursive case: Define the problem in terms of smaller subproblems. Break the problem down into smaller versions of itself, and call the function recursively to solve each subproblem.

* Step3 - Ensure the recursion terminates: Make sure that the recursive function eventually reaches the base case, and does not enter an infinite loop.

* step4 - Combine the solutions: Combine the solutions of the subproblems to solve the original problem.


## 2.1 A Mathematical Interpretation

Let us consider a problem that a programmer has to determine the sum of first n natural numbers, there are several ways of doing that but the simplest approach is simply to add the numbers starting from 1 to n. So the function simply looks like this,

```csharp
approach(1) – Simply adding one by one

f(n) = 1 + 2 + 3 +……..+ n
```

There is a simple difference between the approach (1) and approach(2) and that is in approach(2) the function “ f( ) ” itself is being called inside the function, so this phenomenon is named recursion, and the function containing recursion is called recursive function, at the end, this is a great tool in the hand of the programmers to code some problems in a lot easier and efficient way.

## 2.2 How are recursive functions stored in memory?

Recursion uses more memory, because the recursive function adds to the stack with each recursive call, and keeps the values there until the call is finished. The recursive function uses LIFO (LAST IN FIRST OUT) Structure just like the stack data structure. https://www.geeksforgeeks.org/stack-data-structure/
 

## 2.3 What is the base condition in recursion? 
In the recursive program, the solution to the base case is provided and the solution to the bigger problem is expressed in terms of smaller problems. 


```csharp
int fact(int n)
{
    if (n < = 1) // base case
        return 1;
    else    
        return n*fact(n-1);    
}
```
In the above example, the base case for n < = 1 is defined and the larger value of a number can be solved by converting to a smaller one till the base case is reached.


## 2.4 How a particular problem is solved using recursion? 
The idea is to represent a problem in terms of one or more smaller problems, and add one or more base conditions that stop the recursion. For example, we compute factorial n if we know the factorial of (n-1). The base case for factorial would be n = 0. We return 1 when n = 0. 

## 2.5 Why Stack Overflow error occurs in recursion? 
If the base case is not reached or not defined, then the stack overflow problem may arise. Let us take an example to understand this.

```csharp
int fact(int n)
{
    // wrong base case (it may cause
    // stack overflow).
    if (n == 100) 
        return 1;

    else
        return n*fact(n-1);
}
```

If fact(10) is called, it will call fact(9), fact(8), fact(7), and so on but the number will never reach 100. So, the base case is not reached. If the memory is exhausted by these functions on the stack, it will cause a stack overflow error. 

## 2.6 What is the difference between direct and indirect recursion? 
A function fun is called direct recursive if it calls the same function fun. A function fun is called indirect recursive if it calls another function say fun_new and fun_new calls fun directly or indirectly. The difference between direct and indirect recursion has been illustrated in Table 1. 

```csharp
// An example of direct recursion
void directRecFun()
{
    // Some code....

    directRecFun();

    // Some code...
}

// An example of indirect recursion
void indirectRecFun1()
{
    // Some code...

    indirectRecFun2();

    // Some code...
}
void indirectRecFun2()
{
    // Some code...

    indirectRecFun1();

    // Some code...
}
```

## 2.7 What is the difference between tailed and non-tailed recursion? 
A recursive function is tail recursive when a recursive call is the last thing executed by the function. Please refer tail recursion article for details. 

## 2.8 How memory is allocated to different function calls in recursion? 
When any function is called from main(), the memory is allocated to it on the stack. A recursive function calls itself, the memory for a called function is allocated on top of memory allocated to the calling function and a different copy of local variables is created for each function call. When the base case is reached, the function returns its value to the function by whom it is called and memory is de-allocated and the process continues.
Let us take the example of how recursion works by taking a simple function. 

```csharp
// A C# program to demonstrate
// working of recursion
using System;
 
class GFG {
 
    // function to demonstrate
    // working of recursion
    static void printFun(int test)
    {
        if (test < 1)
            return;
        else {
            Console.Write(test + " ");
 
            // statement 2
            printFun(test - 1);
 
            Console.Write(test + " ");
            return;
        }
    }
 
    // Driver Code
    public static void Main(String[] args)
    {
        int test = 3;
        printFun(test);
    }
}
 
```
Output：```3 2 1 1 2 3```
Time Complexity: O(1)
Auxiliary Space: O(1)

When printFun(3) is called from main(), memory is allocated to printFun(3) and a local variable test is initialized to 3 and statement 1 to 4 are pushed on the stack as shown in below diagram. It first prints ‘3’. In statement 2, printFun(2) is called and memory is allocated to printFun(2) and a local variable test is initialized to 2 and statement 1 to 4 are pushed into the stack. Similarly, printFun(2) calls printFun(1) and printFun(1) calls printFun(0). printFun(0) goes to if statement and it return to printFun(1). The remaining statements of printFun(1) are executed and it returns to printFun(2) and so on. In the output, values from 3 to 1 are printed and then 1 to 3 are printed. The memory stack has been shown in below diagram.
![](https://media.geeksforgeeks.org/wp-content/cdn-uploads/recursion.jpg)

# 3 Recursion VS Iteration

| SR No. | Recursion | Iteration  |
|:------:|:------|:------|
| 1)     | Terminates when the base case becomes true. | Terminates when the condition becomes false. |
| 2)     | Used with functions. | Used with loops. |
| 3)     | Every recursive call needs extra space in the stack memory. | Every iteration does not require any extra space. |
| 4)     | Smaller code size. | Larger code size. |
Now, let’s discuss a few practical problems which can be solved by using recursion and understand its basic working. For basic understanding please read the following articles. 



# 4 Problems
## 4.1 Problem 1: Find Fibonacci Series
Write a program and recurrence relation to find the Fibonacci series of n where n>2 . 
Mathematical Equation:  
```
n if n == 0, n == 1;      
fib(n) = fib(n-1) + fib(n-2) otherwise;
```
Recurrence Relation: 
```
T(n) = T(n-1) + T(n-2) + O(1)
```
Recursive program: 
```
Input: n = 5 
Output:
Fibonacci series of 5 numbers is : 0 1 1 2 3
```
Output
```
Fibonacci series of 5 numbers is: 0 1 1 2 3 
```

## 4.2 Problem 2: Find the Factorial of n

Write a program and recurrence relation to find the Factorial of n where n>2 . 
Mathematical Equation: 
```
1 if n == 0 or n == 1;      
f(n) = n*f(n-1) if n> 1;
```
Recurrence Relation: 
```
T(n) = 1 for n = 0
T(n) = 1 + T(n-1) for n > 0
```

Recursive Program: 
```
Input: n = 5 
Output: 
factorial of 5 is: 120
```
Implementation: 
