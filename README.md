# 内容简介

本书是关于机器学习的各类文档，主要内容如下所示：

* 创建日期：2023/04/30
* 内容简介：本书介绍了机器学习的相关内容与实践。

* 操作手册：**[Gitbook 使用说明(飞书)](https://i8bbmkcybg.feishu.cn/docx/QgCoddeqwoAnxNxmr76cgHc1npf) 使用飞书编写的详细文档。**

* 仓库位置：` https://gitee.com/hwaust/ebook-algorithms.git`
* 访问地址：http://121.199.10.158:8107/ebooks/ebook-algorithms/_book/



# 基本操作说明

## 仓库建立

```bash
cd c:\gitee.com
git clone https://gitee.com/hwaust/ebook-algorithms.git
ii ebook-algorithms
```

## 内容编写

使用 Typora 打开目录 `c:\gitee.com\ebook-algorithms` 进行编写即可。

## 上传更新

编写好以后有2个重要脚本：

1. `commit.bat "commit message"` 上传更新，用于将本更新上传至git仓库。
    * 注1：在Windows上是 `commit.bat`，而在Linux上是`commit.bat`。
    * 注2：这个脚本可以多次调用。

2. `compile_remote` 远程编译，在远程更新电子书，包括下载仓库和编译脚本两步。








## 相关Git命令

```bash
# 下载仓库
git remote add origin https://gitee.com/hwaust/machine-learning-ebook.git
git push -u origin "master"

# 长期存储密码，只需第1次输入即可完成。
git config --global credential.helper store
```

操作示例

```bash
root@server00:~/myweb/ROOT/ebooks/algorithms# git config --global credential.helper store
root@server00:~/myweb/ROOT/ebooks/algorithms# git pull
Username for 'https://gitee.com': hwaust@126.com
Password for 'https://hwaust@126.com@gitee.com':
Already up to date.
root@server00:~/myweb/ROOT/ebooks/algorithms# git pull
Already up to date.
root@server00:~/myweb/ROOT/ebooks/algorithms#
```






# 参考书籍

## **《深度学习》**（异步图书）

Ian Goodfellow(伊恩·古德费洛) (作者), Yoshua Bengio(约书亚·本吉奥) (作者), Aaron Courville(亚伦·库维尔) (作者) 

**内容简介**
《深度学习》由全球知名的三位专家Ian Goodfellow、Yoshua Bengio 和Aaron Courville撰写，是深度学习领域奠基性的经典教材。
全书的内容包括3个部分：

* 第1部分 数学工具和机器学习的概念，简介深度学习的预备知识；
* 第2部分 系统深入地讲解现今已成熟的深度学习方法和技术；
* 第3部分 讨论某些具有前瞻性的方向和想法，它们被公认为是深度学习未来的研究重点。

《深度学习》适合各类读者阅读，包括相关专业的大学生或研究生，以及不具有机器学习或统计背景、但是想要快速补充深度学习知识，以便在实际产品或平台中应用的软件工程师。

**作者简介**

* **Ian Goodfellow** 谷歌公司(Google) 的研究科学家，2014年蒙特利尔大学机器学习博士。他的研究兴趣涵盖大多数深度学习主题，特别是生成模型以及机器学习的安全和隐私。Ian Goodfellow 在研究对抗样本方面是一位有影响力的早期研究者，他发明了生成式对抗网络，在深度学习领域贡献卓越。
* **Yoshua Bengio** 蒙特利尔大学计算机科学与运筹学系(DIRO) 的教授，蒙特利尔学习算法研究所(MILA) 的负责人，CIFAR 项目的共同负责人，加拿大统计学习算法研究主席。Yoshua Bengio 的主要研究目标是了解产生智力的学习原则。他还教授“机器学习”研究生课程(IFT6266)，并培养了一大批研究生和博士后。
* **Aaron Courville** 蒙特利尔大学计算机科学与运筹学系的助理教授，也是LISA 实验室的成员。目前他的研究兴趣集中在发展深度学习模型和方法，特别是开发概率模型和新颖的推断方法。Aaron Courville 主要专注于计算机视觉应用，在其他领域，如自然语言处理、音频信号处理、语音理解和其他AI 相关任务方面也有所研究。

**中文版审校者简介**
张志华，北京大学数学科学学院统计学教授，北京大学大数据研究中心和北京大数据研究院数据科学教授，主要从事机器学习和应用统计学的教学与研究工作。

**译者简介**

* 赵申剑，上海交通大学计算机系硕士研究生，研究方向为数值优化和自然语言处理。
* 黎彧君，上海交通大学计算机系博士研究生，研究方向为数值优化和强化学习。
* 符天凡，上海交通大学计算机系硕士研究生，研究方向为贝叶斯推断。
* 李凯，上海交通大学计算机系博士研究生，研究方向为博弈论和强化学习。

**蒙特利尔大学简介**([百度百科](https://baike.baidu.com/item/%E8%92%99%E7%89%B9%E5%88%A9%E5%B0%94%E5%A4%A7%E5%AD%A6/6663922?fr=aladdin))
蒙特利尔大学（Université de Montréal；魁北克法语读音：[ynivɛʁsi̥te dœ ˈmɔ̃.ˌʁe.al]）是一所主校区位于加拿大魁北克省蒙特利尔市的以法语为主要教学语言、提供少量以英语或西班牙语授课的课（Cours）或课程（Programme） [134-135] [161-163]的公立研究型大学，其前身是由罗马教廷创办于一八七八年的拉瓦尔大学蒙特利尔校区（Université Laval à Montréal），并于一九一九年五月八日独立而变更为现名 [17] 。

该校由主校区、布霍萨荷（Brossard）校区 [172] 、中心（MIL）校区 [157] 、拉瓦尔（Laval）校区、圣·亚桑特（Saint-Hyacinthe）校区、牟希西（Mauricie）校区、隆格热（Longueuil）校区、拉㝹地爱荷（Lanaudière）校区组成。另外，蒙特利尔工学院 [159] 和蒙特利尔高等商学院是根据与蒙特利尔大学之间的合作协议确立的在行政与财政上拥有自治权的附属学院 [169-170]。

蒙特利尔大学在2023年泰晤士高等教育世界大学排名中，**世界排名为第111名** [37] ，而在二〇二二年麦克林杂志的大学排名中则是排在加拿大国内第9名的医博类大学 [171] ，其科研经费总额在全加拿大位列第三 [1] ，它同时也是世界上规模最大的法语大学 [2] 以及法语地区中学术水平名列前茅的公立的法语综合性大学，是加拿大U15研究型大学联盟、国际公立大学论坛、加拿大研究型图书馆协会成员 [3] ，学校的科研体量在加拿大位列第四 [1] ，并培育过众多影响魁北克省本地、加拿大国内甚至世界的各领域要人。