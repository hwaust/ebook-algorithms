@echo off
set git_repo=ebook-algorithms
set gitee=https://gitee.com/hwaust/
set url=http://121.199.10.158:8107/ebooks/

echo *************************************************
echo updating : %gitee%%git_repo%
ssh ali "cd /root/myweb/ROOT/ebooks/%git_repo%; pwd; git pull; gitbook build"
echo ebook url: %url%%git_repo%/_book/
echo *************************************************
