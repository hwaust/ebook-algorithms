---
# YAML block.
title: "第1章 算法简介"
numbersections: true
author: Dr.Hao
date: December 16, 2020
pandoc_args: ["--toc", "--toc-depth=4"]
toc:
  depth_from: 1
  depth_to: 4
  ordered: false  
html:
  embed_local_images: true
  embed_svg: true
  offline: false
export_on_save:
  pandoc: true
  html: true
---
# 学习目标


# 主要内容

# 参考资料
算法入门 https://haolaoshi.blog.csdn.net/article/details/105770539