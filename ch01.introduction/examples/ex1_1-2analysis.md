---
# YAML block.
title: "分析"
numbersections: true
author: Dr.Hao
date: December 16, 2020
pandoc_args: ["--toc", "--toc-depth=4"]
toc:
  depth_from: 1
  depth_to: 4
  ordered: false  
html:
  embed_local_images: true
  embed_svg: true
  offline: false
export_on_save:
  pandoc: true
  html: true
---

# 示例1：随机选人

要求：随机从教室里选取班级为X, 编号为Y的同学,并在屏幕上显示.如：1班13号同学。

* 问题分析

    * 分析问题
    * 班级的个数，班级个数并不确定班级的人数，每个班级人数也不确定随机数的产生
    * 建立相应的数学模型
    * 算法分析与选择

* 算法表示

    * 算法分析
        * 班级人数信息 `int number[x]={y0,y1,y2…,yn};`
    * 算法实现

    建立中间变量Int class = 0;Int id = 0;进行随机选择处理得到结果保存到Class和id中

* 程序实现

    输出结果printf("Class: %d, ID %d has been SELECTED!", class, id);

    * 程序调试
    * 输出存储

