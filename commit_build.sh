####################################################
#    Initial: commit & variables declaration       #
####################################################
git_repo="ebook-algorithms"
gitee="https://gitee.com/hwaust/"
url="http://121.199.10.158:8107/ebooks/"
echo "************* Functionality ****************"
echo "1. Local:  git commit and push"
echo "2. Remote: git pull & gitbook build"



####################################################
#    Step 1: local commit & push local changes     #
####################################################
git add .
git commit -m "$1"
git push



####################################################
#    Step 2: remote refresh & gitbook build        # 
####################################################
echo "updating : $gitee$git_repo"
ssh ali "cd /root/myweb/ROOT/ebooks/$git_repo; pwd; git pull; gitbook build"
echo "visit url: $url$git_repo/_book/"
echo "*************************************************"
sleep 10s