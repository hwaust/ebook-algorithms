---
# YAML block.
title: "2.1 递归调用"
numbersections: true
author: Dr.Hao
date: December 16, 2020
pandoc_args: ["--toc", "--toc-depth=4"]
toc:
  depth_from: 1
  depth_to: 4
  ordered: false  
html:
  embed_local_images: true
  embed_svg: true
  offline: false
export_on_save:
  pandoc: true
  html: true
---
# 2.1 Recursion

Have you ever seen a set of Russian dolls? At first, you see just one figurine, usually painted wood, that looks something like this:

![img](./images/2101.jpg)

You can remove the top half of the first doll, and what do you see inside? Another, slightly smaller, Russian doll!

![img](./images/2102.jpg)

You can remove that doll and separate its top and bottom halves. And you see yet another, even smaller, doll:

![img](./images/2103.jpg)

And once more:

![img](./images/2104.jpg)

And you can keep going. Eventually you find the teeniest Russian doll. It is just one piece, and so it does not open:

![img](./images/2105.jpg)

We started with one big Russian doll, and we saw smaller and smaller Russian dolls, until we saw one that was so small that it could not contain another.

What do Russian dolls have to do with algorithms? Just as one Russian doll has within it a smaller Russian doll, which has an even smaller Russian doll within it, all the way down to a tiny Russian doll that is too small to contain another, we'll see how to design an algorithm to solve a problem by solving a smaller instance of the same problem, unless the problem is so small that we can just solve it directly. We call this technique **recursion**.

Recursion has many, many applications. In this module, we'll see how to use recursion to compute the factorial function, to determine whether a word is a palindrome, to compute powers of a number, to draw a type of fractal, and to solve the ancient Towers of Hanoi problem. Later modules will use recursion to solve other problems, including sorting.



# The factorial function

For our first example of recursion, let's look at how to compute the factorial function. We indicate the factorial of  by . It's just the product of the integers 1 through . For example, 5! equals , or 120. (Note: Wherever we're talking about the factorial function, all exclamation points refer to the factorial function and are not for emphasis.)



You might wonder why we would possibly care about the factorial function. It's very useful for when we're trying to count how many different orders there are for things or how many different ways we can combine things. For example, how many different ways can we arrange  things? We have  choices for the first thing. For each of these  choices, we are left with  choices for the second thing, so that we have  choices for the first two things, in order. Now, for each of these first two choices, we have  choices for the third thing, giving us  choices for the first three things, in order. And so on, until we get down to just two things remaining, and then just one thing remaining. Altogether, we have  ways that we can order  things. And that product is just  ( factorial), but with the product written going from  down to 1 rather than from 1 up to .



Another use for the factorial function is to count how many ways you can choose things from a collection of things. For example, suppose you are going on a trip and you want to choose which T-shirts to take. Let's say that you own  T-shirts but you have room to pack only  of them. How many different ways can you choose  T-shirts from a collection of  T-shirts? The answer (which we won't try to justify here) turns out to be . So the factorial function can be pretty useful. You can learn more about [permutations and combinations here](https://www.khanacademy.org/v/permutations), but you don't need to understand them to implement a factorial algorithm.



The factorial function is defined for all positive integers, along with 0. What value should 0! have? It's the product of all integers greater than or equal to 1 and less than or equal to 0. But there are no such integers. Therefore, we define 0! to equal the identity for multiplication, which is 1. (Defining 0! = 1 meshes nicely with the formula for choosing  things out of  things. Suppose that we want to know how many ways there are to choose  things out of  things. That's easy, because there is only one way: choose all  things. So now we know that, using our formula,  should equal 1. But  is 0!, so now we know that  should equal 1. If we cancel out the  in both the numerator and denominator, we see that  should equal 1, and it does because 0! equals 1.)



So now we have a way to think about . It equals 1 when , and it equals  when  is positive.