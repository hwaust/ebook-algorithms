/*
作者：郝伟老师

作用：实验验证社会财富的分配原则

参数： 
    num 进行 num 万次测试，缺省值为100.

编译：
    javac -encoding UTF-8 SocialPropertySimulation.java; java SocialPropertySimulation


【更新历史】
2024/02/23 创建此文件

*/

import java.util.Arrays;
import java.util.Random;

public class SocialPropertySimulation {

	public static void main(String[] args) {
        int testTimes = args.length > 0 ? Integer.parseInt(args[0]) : 100;
        test1(testTimes*10000);
	}

    static Random random = new Random();
    public static void test1(int testTimes){
        int[] ps = new int[100];
        // 进行100万次测试
		for (int i = 1; i <= testTimes; i++) {
            // 随机发生1次交易
			for (int j = 0; j < ps.length; j++) {
				int pos = random.nextInt(ps.length);
				ps[j] -= 1;
				ps[pos] += 1;
			}
            if(i % 100000 == 0) System.out.print(".");
            if(i % 1000000 == 0) System.out.println( i/10000 + "万次");
		}

		Arrays.parallelSort(ps);
		System.out.println();
		for (int j = 0; j < ps.length; j++) {
			System.out.printf("第 %3d 人的财富： %8d元.\n", j+1, ps[j]);
		}
        System.out.println("总测试次数：" + testTimes + "次");
    }

}

